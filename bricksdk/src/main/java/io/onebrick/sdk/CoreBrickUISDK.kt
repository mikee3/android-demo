package io.onebrick.sdk

import android.content.Context
import android.content.Intent
import android.util.Log
import io.onebrick.sdk.model.AuthenticateUserResponseData
import io.onebrick.sdk.ui.common.LandingActivity
import io.onebrick.sdk.util.Environment

interface ICoreBrickUISDK  {
    fun onTransactionSuccess(transactionResult: AuthenticateUserResponseData)
}

class CoreBrickUISDK {

    companion object {
        fun initializedUISDK(
                context: Context,
                clientId: String,
                secret: String,
                name:String,
                url:String,
                environment: Environment) {

            CoreBrickSDK.initializedSDK(clientId, secret, name, url, environment)

            val brickCoreUIIntent = Intent()
            brickCoreUIIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            brickCoreUIIntent.setClass(context, LandingActivity::class.java)
            context.startActivity(brickCoreUIIntent)
        }

        fun delegatingBackResult(result: AuthenticateUserResponseData, context: Context) {
            var coreUIInterface: ICoreBrickUISDK? = null
            if(context is ICoreBrickUISDK) coreUIInterface = this as ICoreBrickUISDK
            Log.v("CORE_UI", coreUIInterface.toString())
            coreUIInterface?.onTransactionSuccess(result)
        }


    }
}